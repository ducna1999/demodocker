﻿namespace DemoDocker.Dto
{
    public class LoginResult
    {
        public bool Status { get; set; }
        public string Message { get; set; }
        public string Token { get; set; }
    }
}
