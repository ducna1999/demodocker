﻿using AutoMapper;
using DemoDocker.Dto;
using DemoDocker.Models;
using DemoDocker.Services.Interface;
using MediatR;
using System.Security.Cryptography;
using System.Text;

namespace DemoDocker.Commands.AccountCommand
{
    public class LoginCommandHandler : IRequestHandler<LoginCommand, LoginResult>
    {
        private readonly DemoDockerDbContext context;
        private readonly ILogger<LoginCommand> logger;
        private readonly IMapper mapper;
        private readonly IAuthService authService;
        private readonly IHttpContextAccessor httpContextAccessor;

        public LoginCommandHandler(DemoDockerDbContext context, ILogger<LoginCommand> logger, IMapper mapper, IAuthService authService, IHttpContextAccessor httpContextAccessor)
        {
            this.context = context;
            this.logger = logger;
            this.mapper = mapper;
            this.authService = authService;
            this.httpContextAccessor = httpContextAccessor;
        }

        public async Task<LoginResult> Handle(LoginCommand request, CancellationToken cancellationToken)
        {
            LoginResult result = new LoginResult();

            if (string.IsNullOrEmpty(request.UserName) || string.IsNullOrEmpty(request.Password))
            {
                result.Status = false;
                result.Message = "Tài khoản và mật khẩu không được để trống";
                return result;
            }

            using (SHA256 sha256 = SHA256.Create())
            {
                byte[] hashedBytes = sha256.ComputeHash(Encoding.UTF8.GetBytes(request.Password));
                string hashedPassword = BitConverter.ToString(hashedBytes).Replace("-", "").ToLower();

                var user = context.User.FirstOrDefault(p => p.UserName == request.UserName && p.Password == hashedPassword);

                if (user == null)
                {
                    result.Status = false;
                    result.Message = "Sai tên đăng nhập hoặc mật khẩu";
                    return result;
                }

                //result.User = mapper.Map<UserDto>(check);
                result.Status = true;
                result.Token = await authService.GenerateToken();

                authService.SaveUserLogin(user);

                return result;
            }
        }
    }
}
