﻿using DemoDocker.Dto;
using MediatR;

namespace DemoDocker.Commands.AccountCommand
{
    public class LoginCommand : IRequest<LoginResult>
    {
        public string UserName { get; set; }
        public string Password { get; set; }
    }
}
