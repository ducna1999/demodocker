﻿using Microsoft.EntityFrameworkCore;

namespace DemoDocker.Models
{
    public class DemoDockerDbContext : DbContext
    {
        public DemoDockerDbContext(DbContextOptions<DemoDockerDbContext> options) : base(options)
        {

        }

        public DbSet<User> User { get; set; }
    }
}
