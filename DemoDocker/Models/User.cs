﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace DemoDocker.Models
{
    [Table("UserInfo")]
    public class User
    {
        [Column("id")]
        public int Id { get; set; }

        [Column("username")]
        [StringLength(36)]
        public string UserName { get; set; } = string.Empty;

        [Column("password")]
        [StringLength(256)]
        public string Password { get; set; } = string.Empty;
    }
}
