﻿using System.ComponentModel.DataAnnotations.Schema;

namespace DemoDocker.Models
{
    public class BaseEntity
    {
        [Column("ngtao")]
        public int UserId { get; set; }

        [Column("ntao")]
        public DateTime NgayTao_CapNhat { get; set; } = DateTime.Now;
    }
}
