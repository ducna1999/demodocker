﻿using DemoDocker.Commands.AccountCommand;
using DemoDocker.Dto;
using DemoDocker.Services.Interface;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace DemoDocker.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LoginController : ControllerBase
    {
        private readonly IConfiguration configuration;
        private readonly IAuthService authService;
        private readonly IMediator mediator;
        public LoginController(IConfiguration configuration, IMediator mediator, IAuthService authService)
        {
            this.configuration = configuration;
            this.mediator = mediator;
            this.authService = authService;
        }

        [AllowAnonymous]
        [HttpPost]
        public async Task<IActionResult> Login(LoginCommand command)
        {
            IActionResult res = Unauthorized();
            var returnUser = await mediator.Send(command);
            if (returnUser != null)
            {
                res = Ok(returnUser);
            }

            return res;
        }
    }
}
