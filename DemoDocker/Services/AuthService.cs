﻿using DemoDocker.Models;
using DemoDocker.Services.Interface;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json.Linq;
using System.IdentityModel.Tokens.Jwt;
using System.Text;
using System.Text.Json;

namespace DemoDocker.Services
{
    public class AuthService : IAuthService
    {
        private readonly IConfiguration configuration;
        private readonly IHttpContextAccessor httpContextAccessor;
        public AuthService(IConfiguration configuration, IHttpContextAccessor httpContextAccessor)
        {
            this.configuration = configuration;
            this.httpContextAccessor = httpContextAccessor;

        }
        public async Task<string> GenerateToken()
        {
            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(configuration["Jwt:Key"]));
            var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);

            var token = new JwtSecurityToken(configuration["Jwt:Issuer"],
                configuration["Jwt:Audience"],
                null,
                expires: DateTime.Now.AddMinutes(1),
                signingCredentials: credentials);

            return new JwtSecurityTokenHandler().WriteToken(token);
        }

        public User GetUserLogin()
        {
            string juser = httpContextAccessor.HttpContext.Session.GetString("UserLogin");
            if (!string.IsNullOrEmpty(juser))
                return JsonSerializer.Deserialize<User>(juser);
            else return null;
        }

        public void SaveUserLogin(User user)
        {
            httpContextAccessor.HttpContext.Session.SetString("UserLogin", JObject.FromObject(user).ToString());
        }
    }
}
