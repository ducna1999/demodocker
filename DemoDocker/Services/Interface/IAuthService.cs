﻿using DemoDocker.Models;

namespace DemoDocker.Services.Interface
{
    public interface IAuthService
    {
        Task<string> GenerateToken();
        void SaveUserLogin(User user);
        User GetUserLogin();
    }
}
